# Preparation

on lxplus8.cern.ch

```
export CLUSTER_NAME="<cluster name>"

export KUBECONFIG=$HOME/cern/openstack/$CLUSTER_NAME/config

kubectl get nodes

kubectl label nodes <node>-# notice.node.role=true
```
Detailed informaton on [ creating a K8s cluster ](https://gitlab.cern.ch/cmsos/kube/-/wikis/Creating%20a%20Kubernetes%20cluster%20in%20OpenStack) on IT services

The notice example requires [Opensearch services installed](https://gitlab.cern.ch/cmsos/kube/-/wikis/Installing%20Opensearch%20in%20Openstack) within the cluster.

# Helm commands

helm repo add notice-devel https://gitlab.cern.ch/api/v4/projects/164726/packages/helm/devel

helm repo update

helm search repo --devel

### Uninstallation/installation on general purpose cluster
```
helm uninstall notice
helm install notice notice-devel/cmsos-notice-helm --devel
```

### Uninstallation/installation in CMS cluster

```
helm uninstall notice -n xdaq
helm install --set zone.name=xdaq --set zone.createnamespace=false --set zone.imagepullsecrets=false notice notice-devel/cmsos-notice-helm --devel -n xdaq
```

# kubectl commands

kubectl get pods -n notice

kubectl get services -n notice -o wide

kubectl exec -ti <podid> -n notice -- bash

# browse with manual proxy at localhost 1082

ssh -ND 1082 lxplus.cern.ch

http://k8sbox-bnx7wmqhhrjl-node-2.cern.ch:32000/urn:xdaq-application:lid=50


# opensearch related 

bash into a pod

curl -XGET http://opensearch-cluster-master.default:9200/_cat/indices

curl http://opensearch-cluster-master.default:9200/_data_stream?pretty

curl -XGET "http://opensearch-cluster-master.default:9200/datastream.notice.exception/_count"

example
curl -XGET "http://opensearch-cluster-master.default:9200/.ds-datastream.notice.exception-000018/_count"



# Insert document

```
curl -XPOST -H 'Content-Type: application/json' "http://opensearch-cluster-master.default:9200/datastream.notice.exception/_doc" -d '{....}}'
```

# Get all hits

`curl -XGET http://opensearch-cluster-master.default:9200/datastream.notice.exception/_search`


# Get latest

`curl -X POST "http://opensearch-cluster-master.default:9200/datastream.notice.exception/_search?pretty" -H 'Content-Type: application/json' -d '{"query": {"match_all": {}},"size": 1,"sort": [{"@timestamp": {"order": "desc"}}]}'`

# Accessing EMQ X Dashboard

Check which nodeport is used by emqx service:

```
$ kubectl describe svc emqx -n notice | grep NodePort
Type:                     NodePort
NodePort:                 dashboard  31268/TCP
```

Choose one of the nodes of Kubernetes cluster:

```
$ kubectl get nodes
NAME                          STATUS   ROLES    AGE     VERSION
devel-zptgn6ang2vf-master-0   Ready    master   2d19h   v1.25.3
devel-zptgn6ang2vf-node-0     Ready    <none>   2d19h   v1.25.3
devel-zptgn6ang2vf-node-1     Ready    <none>   2d19h   v1.25.3
devel-zptgn6ang2vf-node-2     Ready    <none>   2d19h   v1.25.3
```

Use this URL to open EMQ X Dashboard:

`http://devel-zptgn6ang2vf-node-0:31268`

First time use username: "admin" and password: "public"

## Direct subscrition to EMQX broker
Retrieve the pod name for the source application

`kubectl get pods -n notice`

E.g.
```
...
source-8566cf5d74-8dktg    1/1     Running   0          15m
....
```

Bash into the application pod source

`kubectl exec -ti source-8566cf5d74-8dktg  -n notice -- bash`

Subscribe using the mosquitto command line

`mosquitto_sub -h notice -t 'notice/exception/+' --verbose -q 2 -c -i myName`


## Running on daq3val K8s cluster

### Prepare cluster

```
kubectl config use-context kubernetes-admin@kubernetes
kubectl get nodes
kubectl label nodes d3vrubu-c2e34-06-01  notice.node.role=true
kubectl label nodes d3vrubu-c2e34-08-01  notice.node.role=true
kubectl label nodes d3vrubu-c2e34-10-01  notice.node.role=true
```


`git clone https://gitlab.cern.ch/cmsos/kube.git`

### install openebs

```
cd kube/openebs
helm install openebs --namespace openebs openebs-3.3.1.tgz --create-namespace
```


### install opensearch

```
cd kube/opensearch
helm install -f values.yaml opensearch opensearch-2.8.0.tgz
kubectl get pv
kubectl get pvc
```


### install notice

```
helm repo add notice-devel https://gitlab.cern.ch/api/v4/projects/164726/packages/helm/devel
helm install notice notice-devel/cmsos-notice-helm --devel
```

### Running Opensearch in Sandbox Kubernetes cluster prepared by sysadmins

1. The cluster has opensearch operator installed: https://gitlab.cern.ch/cms-daq-sysadmins/kubernetes/example-apps/-/tree/master/charts/opensearch-opensearch-operator

2. The information on how to use this operator is documented at https://github.com/opensearch-project/opensearch-k8s-operator/blob/main/docs/userguide/main.md

## Building Helm chart from example-apps

a. git clone https://gitlab.cern.ch/cms-daq-sysadmins/kubernetes/example-apps.git

b. cd example-apps/charts/opensearch-opensearch-operator

c. helm package --version "1.0.0" --destination chart .

d. helm install opensearch-cluster chart/opensearch-opensearch-operator-1.0.0.tgz -n xdaq

## Using Helm chart from upstream

a. Configuration of opensearch cluster can be done through Helm chart: https://github.com/opensearch-project/opensearch-k8s-operator/blob/main/docs/userguide/cluster-chart.md

b. Pulling Helm chart
```
helm repo add opensearch-operator https://opensearch-project.github.io/opensearch-k8s-operator/
helm pull opensearch-operator/opensearch-cluster
```
The file opensearch-cluster-2.7.0.tgz is downloaded to the local file system.

c. Installing Helm chart with a desired opensearch cluster Configuration
```
helm install --set opensearchCluster.general.serviceName=notice-cluster opensearch-cluster opensearch-cluster-2.7.0.tgz -n xdaq
```
