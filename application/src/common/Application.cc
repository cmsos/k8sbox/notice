/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius, P. Tzanis                        *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   		                 *
 *************************************************************************
 */

#include "application/Application.h"

XDAQ_INSTANTIATOR_IMPL(application::Application);

application::Application::Application(xdaq::ApplicationStub * s): xdaq::Application(s), fsm_("application-workloop")
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Hello Folks!");
	// used to make a continuous notification
	fsm_.addState('R', "run", this,  &application::Application::empty);
	fsm_.addStateTransition('R', 'R', "notify",   this,    &application::Application::notification);
	fsm_.setInitialState('R');  
	fsm_.reset();
	toolbox::Event::Reference e(new toolbox::Event("notify",this));
	fsm_.fireEvent(e);

}

void application::Application::empty (toolbox::fsm::FiniteStateMachine & fsm) 
{
}

void application::Application::notification (toolbox::Event::Reference e)  
{

    XCEPT_DECLARE(xcept::Exception, previous0, "previous exception example message 0");
	XCEPT_DECLARE_NESTED(xcept::Exception, previous1, "previous exception example message 1", previous0);

	std::stringstream msg;
	msg << "Test qualified exception " << e->type();
    XCEPT_DECLARE_NESTED(xcept::Exception, current, msg.str(), previous1);

	this->notifyQualified("error", current);

	::sleep(1);
	
	toolbox::Event::Reference r(new toolbox::Event("notify",this));
	fsm_.fireEvent(r);
}


